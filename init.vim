"" initial configs 

"  ________   _______   ________  ___      ___ ___  _____ ______
" |\   ___  \|\  ___ \ |\   __  \|\  \    /  /|\  \|\   _ \  _   \
" \ \  \\ \  \ \   __/|\ \  \|\  \ \  \  /  / | \  \ \  \\\__\ \  \
"  \ \  \\ \  \ \  \_|/_\ \  \\\  \ \  \/  / / \ \  \ \  \\|__| \  \
"   \ \  \\ \  \ \  \_|\ \ \  \\\  \ \    / /   \ \  \ \  \    \ \  \
"    \ \__\\ \__\ \_______\ \_______\ \__/ /     \ \__\ \__\    \ \__\
"     \|__| \|__|\|_______|\|_______|\|__|/       \|__|\|__|     \|__|

"             |               |   |                     _) |___ \  |  |
"  _` | |   | __|  _ \   __|  __| __ \   _ \  _ \\ \   / | |   ) | |  |    _` |
" (   | |   | |   (   | |     |   | | |  __/  __/ \ \ /  | |  __/ ___ __| (   |
"\__,_|\__,_|\__|\___/ _|    \__|_| |_|\___|\___|  \_/  _|_|_____|   _|  \__,_|
"



noh
syntax on

set t_Co=16
set noshowmode
set showcmd
set ruler
set number
set numberwidth=2
set nohlsearch
set cursorline
set mouse=a
set cursorline
set expandtab
set noshiftround
set lazyredraw
set magic
set hlsearch
set incsearch
set ignorecase
set smartcase
set encoding=utf-8
set modelines=0
set formatoptions=tqn1
set tabstop=4
set shiftwidth=4
set softtabstop=4
set cmdheight=1
set laststatus=2
set backspace=indent,eol,start
set termguicolors
set noswapfile
set nobackup
set list
set listchars=tab:\│\ 
set matchpairs+=<:>
set statusline=%1*\ file\ %3*\ %f\ %4*\ 
set statusline+=%=\ 
set statusline+=%3*\ %l\ of\ %L\ %2*\ line\ 
set scrolloff=20
set nohlsearch
set smartindent
set smarttab
set nowrap
set number
set termbidi
set splitbelow
set splitright
set completeopt=menuone,noselect
set autoindent smartindent
set clipboard+=unnamedplus

let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0
let &t_SI = "\<esc>[5 q"
let &t_SR = "\<esc>[3 q"
let &t_EI = "\<esc>[2 q"

"" PLUGIN MANAGER
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif

call plug#begin('~/.vim/plugged')
" --- lang support 
Plug 'udalov/kotlin-vim'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'tomlion/vim-solidity'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
Plug 'jparise/vim-graphql'
Plug 'ziglang/zig.vim'
Plug 'ray-x/go.nvim'
Plug 'ray-x/guihua.lua' 
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mattn/emmet-vim'

" -- nvim pluggins
Plug 'mhinz/vim-signify'
Plug 'apzelos/blamer.nvim'
Plug 'tpope/vim-fugitive'
Plug 'wincent/ferret'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'dyng/ctrlsf.vim'
Plug 'glepnir/dashboard-nvim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'sharkdp/fd'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-symbols.nvim'
Plug 'liuchengxu/vim-clap'
Plug 'windwp/nvim-autopairs'
Plug 'machakann/vim-sandwich'
Plug 'preservim/nerdcommenter'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'akinsho/bufferline.nvim'
Plug 'BurntSushi/ripgrep'
Plug 'voldikss/vim-floaterm'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'romgrk/fzy-lua-native', { 'do': 'make' }
Plug 'nvim-telescope/telescope-fzf-writer.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'
Plug 'mrjones2014/tldr.nvim'

" -- theme pluggins
Plug 'ryanoasis/vim-devicons'
Plug 'itchyny/lightline.vim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'ap/vim-css-color'
Plug 'sainnhe/sonokai'
call plug#end()

let mapleader = " "
" CUSTOMIZATION
set termguicolors
let g:sonokai_style = 'espresso'
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 1
let g:sonokai_transparent_background = 1
colorscheme sonokai
hi Search guibg=peru guifg=wheat
hi Search cterm=NONE ctermfg=Red ctermbg=LightCyan

" configuration ligthline
let g:lightline = {
\ 'colorscheme': 'sonokai',
\ 'active': {
\   'left': [ [ 'mode', 'paste' ],
\             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
\ },
\ 'component_function': {
\   'gitbranch': 'FugitiveHead'
\ },
\ }

let g:airline_theme = "sonokai"

if has('termguicolors')
  set termguicolors
endif

if !has('gui_running')
  set t_Co=256
endif

filetype plugin indent on
let g:sonokai_transparent_background = 1
let g:sonokai_better_performance = 1
let g:sonokai_current_word = 'bold'
let g:sonokai_diagnostic_virtual_text = 'colored'
let g:sonokai_diagnostic_line_highlight = 1
let g:sonokai_show_eob = 0

"" PLUGIN CONFIG 
hi linenr ctermfg=8
hi cursorline cterm=NONE
hi cursorlinenr ctermfg=15
hi comment ctermfg=8
hi pmenu ctermbg=0 ctermfg=NONE
hi pmenusel ctermbg=4 ctermfg=0
hi pmenusbar ctermbg=0
hi pmenuthumb ctermbg=7
hi matchparen ctermbg=black ctermfg=NONE
hi search ctermbg=0
hi statusline ctermbg=0 ctermfg=NONE
hi statuslinenc ctermbg=0 ctermfg=0
hi user1 ctermbg=1 ctermfg=0
hi user2 ctermbg=4 ctermfg=0
hi user3 ctermbg=0 ctermfg=NONE
hi user4 ctermbg=NONE ctermfg=NONE
hi group1 ctermbg=NONE ctermfg=0
autocmd colorscheme * hi clear cursorline
match group1 /\t/

" configuration signify
set updatetime=10
let g:signify_line_highlight = 0
let g:signify_sign_add                  = '' 
let g:signify_sign_change               = '█' 
let g:signify_sign_add                  = '+'
let g:signify_sign_delete               = '_'
let g:signify_sign_delete_first_line    = '‾'
let g:signify_sign_change               = '!'
let g:signify_sign_change_delete        = g:signify_sign_change . g:signify_sign_delete_first_line
let g:signify_vcs_list                  = [ 'git', 'hg' ]
let g:signify_mapping_next_hunk         = '<leader>gn'
let g:signify_mapping_prev_hunk         = '<leader>gp'
let g:signify_mapping_toggle_highlight  = '<leader>gh'
let g:signify_mapping_toggle            = '<leader>gt'
let g:signify_exceptions_filetype       = [ 'vim', 'c' ]
let g:signify_exceptions_filename       = [ '.vimrc' ]
let g:signify_sign_overwrite            = 1 

let g:signify_sign_color_guifg_add      = '#9cfb9c'
let g:signify_sign_color_guifg_delete   = '#ff8b8b'
let g:signify_sign_color_guifg_change   = '#ffff90'
let g:signify_sign_color_guibg          = '#111111' 

let g:signify_sign_color_ctermfg_add    = 2
let g:signify_sign_color_ctermfg_delete = 1
let g:signify_sign_color_ctermfg_change = 3
let g:signify_sign_color_ctermbg        = 0 

let g:signify_sign_color_group_add    = 'MyAdd'
let g:signify_sign_color_group_delete = 'MyDelete'
let g:signify_sign_color_group_change = 'MyChange'

let g:signify_line_color_add    = 'DiffAdd'
let g:signify_line_color_delete = 'DiffDelete'
let g:signify_line_color_change = 'DiffChange'
let g:signify_enable_cvs = 1
let g:signify_cursorhold_normal = 1
let g:signify_cursorhold_insert = 1 
let g:signify_sign_color_guibg  = '#111111' 

highlight SignifySignAdd    ctermfg=green  guifg=#9cfb9c cterm=NONE gui=NONE
highlight SignifySignDelete ctermfg=red    guifg=#ff8b8b cterm=NONE gui=NONE
highlight SignifySignChange ctermfg=yellow guifg=#ffff90 cterm=NONE gui=NONE
highlight SignColumn        ctermbg=NONE   cterm=NONE    guibg=NONE gui=NONE
highlight link SignifyLineChange DiffText

" configuration blamer
let g:blamer_enabled = 1
let g:blamer_delay = 500
let g:blamer_show_in_visual_modes = 1
let g:blamer_show_in_insert_modes = 1
let g:blamer_template = '<committer> <summary>'
let g:blamer_date_format = '%d/%m/%y'

" configuration notes vim 
let g:notes_directories = ['~/Documents/Notes']

" configuration Coc javascript 
autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear
let g:coc_global_extensions = [
      \ 'coc-tsserver'
  \ ]

if isdirectory('./node_modules') && isdirectory('./node_modules/prettier')
      let g:coc_global_extensions += ['coc-prettier']
endif

if isdirectory('./node_modules') && isdirectory('./node_modules/eslint')
      let g:coc_global_extensions += ['coc-eslint']
endif

function! ShowDocIfNoDiagnostic(timer_id)
    if (coc#float#has_float() == 0 && CocHasProvider('hover') == 1)
         silent call CocActionAsync('doHover')
    endif
endfunction

function! s:show_hover_doc()
    call timer_start(500, 'ShowDocIfNoDiagnostic')
endfunction

autocmd FileType python let b:coc_suggest_disable = 1

autocmd CursorHoldI * :call <SID>show_hover_doc()
autocmd CursorHold * :call <SID>show_hover_doc()

hi Quote ctermbg=109 guifg=#83a598

" configuration ctrip
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard'] "Hide files in .gitignore
let g:ctrlp_show_hidden = 1

" configuration clap
let g:clap_theme = 'palenight'
let g:clap_layout = { 'relative': 'editor' }

" configuration dashboard
let g:dashboard_default_executive ='clap'
highlight dashboardFooter    ctermfg=240
highlight dashboardHeader    ctermfg=114
highlight dashboardCenter    ctermfg=215
highlight dashboardShortCut  ctermfg=245

let g:dashboard_custom_shortcut_icon = {}
let g:dashboard_custom_shortcut_icon['last_session'] = ' '
let g:dashboard_custom_shortcut_icon['find_history'] = 'ﭯ '
let g:dashboard_custom_shortcut_icon['find_file'] = ' '
let g:dashboard_custom_shortcut_icon['new_file'] = ' '
let g:dashboard_custom_shortcut_icon['change_colorscheme'] = ' '
let g:dashboard_custom_shortcut_icon['find_word'] = ' '
let g:dashboard_custom_shortcut_icon['book_marks'] = ''

let g:dashboard_custom_shortcut={
\ 'last_session'       : 'SPC s l',
\ 'find_history'       : 'SPC f h',
\ 'find_file'          : 'SPC f f',
\ 'new_file'           : 'SPC c n',
\ 'change_colorscheme' : 'SPC t c',
\ 'find_word'          : 'SPC f a',
\ 'book_marks'         : 'SPC f b',
\ }

let g:custom_header = [
    \'⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡀⠀⠀⠀⠀⢀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⡖⠁⠀⠀⠀⠀⠀⠀⠈⢲⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠀⠀⠀⠀⠀⣼⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣧⠀⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠀⠀⠀⠀⣸⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣿⣇⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠀⠀⠀⠀⣿⣿⡇⠀⢀⣀⣤⣤⣤⣤⣀⡀⠀⢸⣿⣿⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠀⠀⠀⠀⢻⣿⣿⣔⢿⡿⠟⠛⠛⠻⢿⡿⣢⣿⣿⡟⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠀⣀⣤⣶⣾⣿⣿⣿⣷⣤⣀⡀⢀⣀⣤⣾⣿⣿⣿⣷⣶⣤⡀⠀⠀⠀⠀ ',
    \'⠀⠀⢠⣾⣿⡿⠿⠿⠿⣿⣿⣿⣿⡿⠏⠻⢿⣿⣿⣿⣿⠿⠿⠿⢿⣿⣷⡀⠀⠀ ',
    \'⠀⢠⡿⠋⠁⠀⠀⢸⣿⡇⠉⠻⣿⠇⠀⠀⠸⣿⡿⠋⢰⣿⡇⠀⠀⠈⠙⢿⡄⠀ ',
    \'⠀⡿⠁⠀⠀⠀⠀⠘⣿⣷⡀⠀⠰⣿⣶⣶⣿⡎⠀⢀⣾⣿⠇⠀⠀⠀⠀⠈⢿⠀ ',
    \'⠀⡇⠀⠀⠀⠀⠀⠀⠹⣿⣷⣄⠀⣿⣿⣿⣿⠀⣠⣾⣿⠏⠀⠀⠀⠀⠀⠀⢸⠀ ',
    \'⠀⠁⠀⠀⠀⠀⠀⠀⠀⠈⠻⢿⢇⣿⣿⣿⣿⡸⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠈⠀ ',
    \'⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣼⣿⣿⣿⣿⣧⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ',
    \'⠀⠀⠀⠐⢤⣀⣀⢀⣀⣠⣴⣿⣿⠿⠋⠙⠿⣿⣿⣦⣄⣀⠀⠀⣀⡠⠂⠀⠀⠀ ',
    \'⠀⠀⠀⠀⠀⠈⠉⠛⠛⠛⠛⠉⠀⠀⠀⠀⠀⠈⠉⠛⠛⠛⠛⠋⠁⠀⠀⠀⠀⠀ ',
    \]

let g:NERDCreateDefaultMappings = 1

"" KEY BINDINGS
nmap <C-Z> u
nmap <C-Y> <C-R>

nmap <A-[> :BufferLineCyclePrev<CR>
nmap <A-]> :BufferLineCycleNext<CR>

nmap <A-t> :FloatermToggle<CR>

" move between splits windows
nmap <A-j> <C-w><C-j>
nmap <A-k> <C-w><C-k>
nmap <A-l> <C-w><C-l>
nmap <A-h> <C-w><C-h>

nmap <leader>bc :BufferLinePickClose<CR>
imap <C-Z> <Esc>ua
imap <C-Y> <Esc><C-R>a
imap <Nul> <C-N>

nnoremap <leader>dc <cmd>Clap buffers<cr>

" fugitive
nnoremap <leader>db <cmd>Git blame<cr>
nnoremap <leader>df <cmd>Git diff<cr>
nnoremap <leader>dr <cmd>Gdiffsplit<cr>
" ctrip
nmap <C-F>f <Plug>CtrlSFPrompt                  
nmap <C-F>n <Plug>CtrlSFCwordPath
nmap <C-F>p <Plug>CtrlSFPwordPath

" telescope
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>h <cmd>Telescope keymaps<cr>
nnoremap <leader>t :lua require('telescope').extensions.fzf_writer.files()<CR>
nnoremap <leader>/ :lua require('telescope').extensions.fzf_writer.staged_grep()<CR>
nnoremap <leader>b <cmd>Telescope buffers<cr>
nnoremap <leader>c :lua require('telescope.builtin').git_bcommits()<cr>

" dashboard
nmap <Leader>ss :<C-u>SessionSave<CR>
nmap <Leader>sl :<C-u>SessionLoad<CR>
nmap <Leader>cn :<C-u>DashboardNewFile<CR>

" split documents
nnoremap <leader>n :split \| :2winc h<CR>
nnoremap <leader>o :2winc l \| split \| 2winc h \| norm o<CR>

" coc
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <silent><expr> <C-space> coc#refresh()

"GoTo code navigation
nmap <leader>g <C-o>
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gt <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nmap <leader>rn <Plug>(coc-rename)
nnoremap <silent> <space>d :<C-u>CocList diagnostics<cr>
nnoremap <silent> <space>e :<C-u>CocList extensions<cr>
nnoremap <silent> K :call CocAction('doHover')<CR>

" auto indent
vmap <Tab> >gv
vmap <S-Tab> <gv

" fzf
map ; :Files<CR>

lua << EOF
-- configuration bufferline
require('bufferline').setup {
  options = {
    numbers = "both", 
    close_command = "bdelete! %d",      
    right_mouse_command = "bdelete! %d",
    left_mouse_command = "buffer %d",
    middle_mouse_command = nil,
    buffer_close_icon = '',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',
    show_buffer_icons = true,
    show_buffer_close_icons = false,
    show_close_icon = false,
    show_tab_indicators = true,
    separator_style = "thin",
    always_show_bufferline = true
  }
}

require'nvim-tree'.setup {
  disable_netrw       = true,
  hijack_netrw        = true,
  open_on_setup       = true,
  ignore_ft_on_setup  = {},
  open_on_tab         = false,
  hijack_cursor       = false,
  update_cwd          = false,
  diagnostics = {
    enable = false,
    icons = {
      hint = "",
      info = "",
      warning = "",
      error = "",
    }
  },
  update_focused_file = {
    enable      = false,
    update_cwd  = false,
    ignore_list = {}
  },
  system_open = {
   cmd  = nil,
    args = {}
  },
  view = {
    width = 30,
    height = 30,
    hide_root_folder = false,
    side = 'right',
    mappings = {
      custom_only = false,
      list = {}
    }
  }
}

require('tldr').setup({ tldr_command = 'tldr' })
require('telescope').load_extension('fzy_native')

require("nvim-autopairs").setup{{
  disable_filetype = { "TelescopePrompt" , "vim" },
}}

EOF
nnoremap <silent>[b :BufferLineCycleNext<CR>
nnoremap <silent>b] :BufferLineCyclePrev<CR>

nnoremap <silent><leader>1 <Cmd>BufferLineGoToBuffer 1<CR>
nnoremap <silent><leader>2 <Cmd>BufferLineGoToBuffer 2<CR>
nnoremap <silent><leader>3 <Cmd>BufferLineGoToBuffer 3<CR>
nnoremap <silent><leader>4 <Cmd>BufferLineGoToBuffer 4<CR>
nnoremap <silent><leader>5 <Cmd>BufferLineGoToBuffer 5<CR>
nnoremap <silent><leader>6 <Cmd>BufferLineGoToBuffer 6<CR>
nnoremap <silent><leader>7 <Cmd>BufferLineGoToBuffer 7<CR>
nnoremap <silent><leader>8 <Cmd>BufferLineGoToBuffer 8<CR>
nnoremap <silent><leader>9 <Cmd>BufferLineGoToBuffer 9<CR>

" configuracion nvim-tree
let g:nvim_tree_ignore = [ '.git', 'node_modules', '.cache' ]
let g:nvim_tree_gitignore = 1 
let g:nvim_tree_quit_on_open = 1
let g:nvim_tree_indent_markers = 1
let g:nvim_tree_hide_dotfiles = 1
let g:nvim_tree_git_hl = 1
let g:nvim_tree_highlight_opened_files = 1
let g:nvim_tree_root_folder_modifier = ':~'
let g:nvim_tree_add_trailing = 1
let g:nvim_tree_group_empty = 1 
let g:nvim_tree_disable_window_picker = 1
let g:nvim_tree_icon_padding = ' '
let g:nvim_tree_symlink_arrow = ' >> '
let g:nvim_tree_respect_buf_cwd = 1
let g:nvim_tree_create_in_closed_folder = 0
let g:nvim_tree_refresh_wait = 500
let g:nvim_tree_window_picker_exclude = {
    \   'filetype': [
    \     'notify',
    \     'packer',
    \     'qf'
    \   ],
    \   'buftype': [
    \     'terminal'
    \   ]
    \ }

let g:nvim_tree_special_files = { 'README.md': 1, 'Makefile': 1, 'MAKEFILE': 1 } " List of filenames that gets highlighted with NvimTreeSpecialFile
let g:nvim_tree_show_icons = {
    \ 'git': 1,
    \ 'folders': 0,
    \ 'files': 0,
    \ 'folder_arrows': 0,
    \ }

let g:nvim_tree_icons = {
    \ 'default': '',
    \ 'symlink': '',
    \ 'git': {
    \   'unstaged': "✗",
    \   'staged': "✓",
    \   'unmerged': "",
    \   'renamed': "➜",
    \   'untracked': "★",
    \   'deleted': "",
    \   'ignored': "◌"
    \   },
    \ 'folder': {
    \   'arrow_open': "",
    \   'arrow_closed': "",
    \   'default': "",
    \   'open': "",
    \   'empty': "",
    \   'empty_open': "",
    \   'symlink': "",
    \   'symlink_open': "",
    \   }
    \ }

nnoremap <C-t> :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>
highlight NvimTreeFolderIcon guibg=blue
